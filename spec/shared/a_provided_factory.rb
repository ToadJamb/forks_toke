RSpec.shared_context :factory => true do
  shared_examples 'a provided factory' do |factory, attribute, expected|
    context "given #{factory}" do
      subject { build factory }

      describe "#{attribute}"  do
        it "returns #{expected}" do
          if expected.is_a?(Regexp)
            expect(subject.send(attribute)).to match expected
          elsif expected.is_a?(Class)
            expect(subject.send(attribute)).to be_a expected
          else
            expect(subject.send(attribute)).to eq expected
          end
        end
      end
    end
  end
end
