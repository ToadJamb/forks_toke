require File.expand_path('../../lib/toke', __FILE__)

require File.expand_path('../support/require_quietly', __FILE__)

Dir['spec/support/**/*.rb'].each do |file|
  require File.expand_path("../../#{file}", __FILE__)
end

require File.expand_path('../../lib/toke/factories', __FILE__)

Dir['spec/shared/**/*.rb'].each do |file|
  require File.expand_path("../../#{file}", __FILE__)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.backtrace_exclusion_patterns << %r|/users/.*/\.rvm/gems/|i

  config.disable_monkey_patching!

  config.warnings = true

  config.order = :random

  Kernel.srand config.seed
end
