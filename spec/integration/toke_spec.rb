require 'integration_helper'

RSpec.describe Toke, :vcr do
  describe '.retrieve_token' do
    subject { described_class.retrieve_token params }

    let(:params) {{
      :refresh_token => refresh_token,
      :client_id     => client_id,
      :client_secret => client_secret,
    }}

    let(:refresh_token) { ENV['REFRESH_TOKEN'] }
    let(:client_id)     { ENV['CLIENT_ID'] }
    let(:client_secret) { ENV['CLIENT_SECRET'] }

    it 'gets an access token' do
      expect(subject).to be_a Toke::AccessTokenResponse
      expect(subject.success?).to eq true
      expect(subject.status).to eq 200
      expect(subject.data).to be_a Toke::AccessToken
      expect(subject.data.token).to match(/\w+.*\d+/)
    end

    context 'given a missing parameter' do
      let(:params) { {} }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Toke::AccessTokenResponse
        expect(subject.success?).to eq false
        expect(subject.status).to eq nil
        expect(subject.data).to eq nil
      end
    end

    context 'given an invalid parameter' do
      let(:refresh_token) { 'foo-bar' }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Toke::AccessTokenResponse
        expect(subject.success?).to eq false
        expect(subject.status).to eq 400
        expect(subject.data).to eq nil
      end
    end
  end
end
