require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/vcr_cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  [
    'REFRESH_TOKEN',
    'CLIENT_ID',
    'CLIENT_SECRET',
  ].each do |key|
    config.filter_sensitive_data("<#{key}>") { ENV[key] }
  end
end
