require 'spec_helper'

RSpec.describe "#{Toke::AccessToken} factory", :factory do
  let(:factory) { :toke_access_token }

  it_behaves_like 'a provided factory',
    :toke_access_token, :token, /access-token-\d+/
  it_behaves_like 'a provided factory', :toke_access_token, :expires, 3600

  it_behaves_like 'a provided factory',
    :toke_expired_access_token, :token, /access-token-\d+/
  it_behaves_like 'a provided factory',
    :toke_expired_access_token, :expires, 3600

  it_behaves_like 'a factory attribute', :token, /access-token-\d+/
  it_behaves_like 'a factory attribute', :expires, 1234

  describe 'expired access token factory' do
    subject { build :toke_expired_access_token }
    it 'creates an expired token' do
      expect(subject.expired?).to eq true
    end
  end
end
