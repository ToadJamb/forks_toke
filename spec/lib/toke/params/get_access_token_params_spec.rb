require 'spec_helper'

RSpec.describe Toke::GetAccessTokenParams do
  subject { described_class.new params }

  let(:params) {{
    :client_id     => 'client-id',
    :client_secret => 'client-secret',
    :refresh_token => 'refresh-token',
  }}

  describe '.new' do
    context 'given symbol keys' do
      before { expect(params.keys.all?{ |k| k.is_a? Symbol }).to eq true }

      it 'sets client_id' do
        expect(subject.client_id).to eq 'client-id'
      end

      it 'sets client_secret' do
        expect(subject.client_secret).to eq 'client-secret'
      end

      it 'sets refresh_token' do
        expect(subject.refresh_token).to eq 'refresh-token'
      end
    end

    context 'given string keys' do
      let(:params) {{
        'client_id'     => 'client-id',
        'client_secret' => 'client-secret',
        'refresh_token' => 'refresh-token',
      }}

      it 'sets client_id' do
        expect(subject.client_id).to eq 'client-id'
      end

      it 'sets client_secret' do
        expect(subject.client_secret).to eq 'client-secret'
      end

      it 'sets refresh_token' do
        expect(subject.refresh_token).to eq 'refresh-token'
      end
    end

    shared_examples 'a nil attribute' do |attribute, value|
      describe "##{attribute}" do
        subject { described_class.new(params).send attribute }

        let(:params) {{ attribute => value }}

        context "given #{value.inspect}" do
          it 'returns nil' do
            expect(subject).to eq nil
          end
        end
      end
    end

    it_behaves_like 'a nil attribute', :client_id, nil
    it_behaves_like 'a nil attribute', :client_id, ''
    it_behaves_like 'a nil attribute', :client_id, " \n "

    it_behaves_like 'a nil attribute', 'client_id', nil
    it_behaves_like 'a nil attribute', 'client_id', ''
    it_behaves_like 'a nil attribute', 'client_id', " \n "

    it_behaves_like 'a nil attribute', :refresh_token, nil
    it_behaves_like 'a nil attribute', :refresh_token, ''
    it_behaves_like 'a nil attribute', :refresh_token, " \n "

    it_behaves_like 'a nil attribute', 'refresh_token', nil
    it_behaves_like 'a nil attribute', 'refresh_token', ''
    it_behaves_like 'a nil attribute', 'refresh_token', " \n "

    it_behaves_like 'a nil attribute', :client_secret, nil
    it_behaves_like 'a nil attribute', :client_secret, ''
    it_behaves_like 'a nil attribute', :client_secret, " \n "

    it_behaves_like 'a nil attribute', 'client_secret', nil
    it_behaves_like 'a nil attribute', 'client_secret', ''
    it_behaves_like 'a nil attribute', 'client_secret', " \n "
  end

  describe '#valid?' do
    context 'given all attributes are set' do
      it 'returns true' do
        expect(subject.valid?).to eq true
      end
    end

    shared_examples 'an invalid attribute' do |attribute|
      context "given #{attribute} is nil" do
        before { params[attribute] = nil }
        before { expect(subject.send(attribute)).to eq nil }

        it 'returns false' do
          expect(subject.valid?).to eq false
        end
      end
    end

    it_behaves_like 'an invalid attribute', :client_id
    it_behaves_like 'an invalid attribute', :client_secret
    it_behaves_like 'an invalid attribute', :refresh_token
  end
end
