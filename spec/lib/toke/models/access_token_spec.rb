require 'spec_helper'

RSpec.describe Toke::AccessToken do
  let(:hash) {{
    'access_token' => 'access-token',
    'expires_in'   => expires_in,
  }}

  let(:expires_in) { 3600 }

  describe '.new' do
    subject { described_class.new hash }

    shared_examples_for 'it sets' do |attr, key, value|
      let(:hash) {{ key => value, }}

      describe "##{attr}" do
        context "given #{key} is #{value.inspect}" do
          it "returns #{value.inspect}" do
            expect(subject.send(attr)).to eq value
          end
        end
      end
    end

    it_behaves_like 'it sets', :token, 'access_token', 'access-token-2'
    it_behaves_like 'it sets', :expires, 'expires_in', 1234
  end

  describe '.expired?' do
    subject { object.expired? }

    let(:object) { described_class.new hash }

    shared_examples_for 'token expiration' do |ago, expires, value|
      context "given the access token was created #{ago} seconds ago" do
        before { allow(Time).to receive(:now).and_return Time.now }
        before { object.instance_variable_set :@created_at, Time.now - ago }

        context "given the token lives for #{expires} seconds" do
          let(:expires_in) { expires }

          it "returns #{value}" do
            expect(subject).to eq value
          end
        end
      end
    end

    it_behaves_like 'token expiration', 3600 - 299, 3600, true
    it_behaves_like 'token expiration', 3600 - 300, 3600, false
    it_behaves_like 'token expiration', 3600 - 301, 3600, false

    it_behaves_like 'token expiration', 360 - 29, 360, true
    it_behaves_like 'token expiration', 360 - 30, 360, false
    it_behaves_like 'token expiration', 360 - 31, 360, false

    it_behaves_like 'token expiration', 120 -  9, 120, true
    it_behaves_like 'token expiration', 120 - 10, 120, false
    it_behaves_like 'token expiration', 120 - 11, 120, false
  end
end
