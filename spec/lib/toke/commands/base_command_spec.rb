require 'spec_helper'

RSpec.describe Toke::BaseCommand do
  subject do
    Module.new do
      extend self
      extend Toke::BaseCommand

      def connection_value
        connection
      end

      def url_value
        url params
      end

      def header_value
        headers
      end

      private

      def endpoint
        'test-point'
      end

      def url_params(params)
        params
      end

      def params
        {'foo' => 'bar'}
      end
    end
  end

  describe '.url' do
    it 'uses the google oauth api' do
      expect(subject.connection_value.build_url.to_s)
        .to match(%r|googleapis.*/oauth2/v3|)
    end

    it 'includes the endpoint' do
      expect(subject.url_value.to_s).to match(%r|/test-point\?|)
    end

    it 'includes url parameters' do
      expect(subject.url_value.to_s).to match(%r|\?.*foo=bar|)
    end
  end

  describe '.headers' do
    it 'contains content type' do
      expect(subject.header_value)
        .to include 'Content-Type' => 'application/json'
    end
  end
end
