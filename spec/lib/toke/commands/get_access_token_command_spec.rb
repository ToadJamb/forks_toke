require 'spec_helper'

RSpec.describe Toke::GetAccessTokenCommand do
  let(:params) {{
    'client_id'     => client_id,
    'client_secret' => client_secret,
    'refresh_token' => refresh_token,
  }}

  let(:client_id)     { 'the-googs' }
  let(:client_secret) { 'client-secret' }
  let(:refresh_token) { 'refresh-token' }
  let(:access_token)  { 'access-token' }

  it "inherits #{Toke::BaseCommand}" do
    expect(described_class.respond_to?(:connection, true)).to eq true
  end

  describe '.execute' do
    subject { described_class.execute params }

    let(:response) { build :reverb_response }

    context 'given valid parameters' do
      let(:response) do
        Faraday::Response.new({
          :status => 200,
          :response_headers => params,
          :body => {
            'access_token' => access_token,
          }.to_json,
        })
      end

      before do
        allow(Toke::GetAccessTokenCommand)
          .to receive(:post)
          .and_return response
      end

      it 'scrubs the client id' do
        expect(subject.inspect).to match client_id

        expect(subject.scrubbed).to_not match client_id
        expect(subject.scrubbed).to match 'CLIENT_ID'
      end

      it 'scrubs the client secret' do
        expect(subject.inspect).to match client_secret

        expect(subject.scrubbed).to_not match client_secret
        expect(subject.scrubbed).to match 'CLIENT_SECRET'
      end

      it 'scrubs the refresh token' do
        expect(subject.inspect).to match refresh_token

        expect(subject.scrubbed).to_not match refresh_token
        expect(subject.scrubbed).to match 'REFRESH_TOKEN'
      end

      it 'scrubs the access token' do
        expect(subject.inspect).to match access_token

        expect(subject.scrubbed).to_not match access_token
        expect(subject.scrubbed).to match 'ACCESS_TOKEN'
      end

      # This may not be a valid scenario, but just in case...
      context 'given no data' do
        let(:access_token_response) { Toke::AccessTokenResponse.new response }

        before do
          allow(Toke::AccessTokenResponse)
            .to receive(:new)
            .and_return access_token_response

          allow(access_token_response).to receive(:data).and_return nil
        end

        it 'does not fail' do
          subject
        end
      end
    end

    context 'given invalid parameters' do
      let(:params) { {} }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Toke::AccessTokenResponse
        expect(subject.status).to eq nil
        expect(subject.success?).to eq false
        expect(subject.data).to eq nil
      end
    end
  end

  describe '.url_params' do
    subject { described_class.url_params get_access_token_params }

    let(:get_access_token_params) { Toke::GetAccessTokenParams.new params }

    it 'includes the client id' do
      expect(subject).to include :client_id => client_id
    end

    it 'includes the client secret' do
      expect(subject).to include :client_secret => client_secret
    end

    it 'includes the refresh token' do
      expect(subject).to include :refresh_token => refresh_token
    end

    it 'includes the grant type' do
      expect(subject).to include :grant_type => 'refresh_token'
    end
  end

  describe '.endpoint' do
    subject { described_class.endpoint }
    it 'sets the endpoint correctly' do
      expect(subject).to eq 'token'
    end
  end
end
