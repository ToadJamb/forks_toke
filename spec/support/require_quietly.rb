module SpecSupport
  extend self

  def require_quietly(path)
    verbose = $VERBOSE
    $VERBOSE = nil
    require path
    $VERBOSE = verbose
  end
end
