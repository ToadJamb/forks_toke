require 'spec_helper'

Dir['spec/integration/support/**/*.rb'].each do |file|
  require File.expand_path("../../#{file}", __FILE__)
end
