Gem::Specification.new do |spec|
  spec.name          = 'toke'
  spec.version       = '0.0.3'
  spec.authors      << 'Travis Herrick'
  spec.authors      << 'Joshua Book'
  spec.email         = ['travish@awesomenesstv.com']
  spec.summary       = 'oauth token retrieval'
  spec.description   = '
    Retrieve OAuth tokens
  '.strip

  spec.homepage      = 'https://github.com/awesomenesstv/toke'
  spec.license       = 'LGPLv3'
  spec.files         = Dir['lib/**/*.rb', 'license/*']

  spec.extra_rdoc_files = [
    'README.md',
    'license/gplv3.md',
    'license/lgplv3.md',
  ]

  spec.add_dependency 'reverb',  '~> 0.0.4'
  spec.add_dependency 'faraday', '~> 0'

  spec.add_development_dependency 'rake_tasks',   '~> 4.1'
  spec.add_development_dependency 'gems',         '~> 0'
  spec.add_development_dependency 'cane',         '~> 2'
  spec.add_development_dependency 'rspec',        '~> 3'
  spec.add_development_dependency 'webmock',      '~> 1'
  spec.add_development_dependency 'vcr',          '~> 2'
  spec.add_development_dependency 'dotenv',       '~> 2'
  spec.add_development_dependency 'factory_girl', '~> 4'
end
