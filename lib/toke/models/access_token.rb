module Toke
  class AccessToken
    attr_accessor :token, :expires

    def initialize(hash = {})
      @token      = hash['access_token']
      @expires    = hash['expires_in']
      @created_at = Time.now
    end

    def expired?
       Time.now > @created_at + expires - (expires / 12)
    end
  end
end
