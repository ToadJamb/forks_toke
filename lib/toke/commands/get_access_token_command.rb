module Toke
  module GetAccessTokenCommand
    extend BaseCommand
    extend self

    def execute(params)
      params = GetAccessTokenParams.new(params)

      response = nil

      if params.valid?
        response = AccessTokenResponse.new(post(params))
        scrub response, params
      else
        response = AccessTokenResponse.new
      end
    end

    def url_params(params)
      {
        :client_id     => params.client_id,
        :client_secret => params.client_secret,
        :refresh_token => params.refresh_token,
        :grant_type    => 'refresh_token',
      }
    end

    def endpoint
      'token'
    end

    private

    def scrub(response, params)
      response.scrub params.client_id,     'CLIENT_ID'
      response.scrub params.client_secret, 'CLIENT_SECRET'
      response.scrub params.refresh_token, 'REFRESH_TOKEN'

      response.scrub (response.data && response.data.token), 'ACCESS_TOKEN'

      response
    end
  end
end
