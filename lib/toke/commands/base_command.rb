module Toke
  module BaseCommand
    extend self

    private

    def connection
      Faraday.new(:url => base_url)
    end

    def post(params)
      connection.post url(params), nil, headers
    end

    def headers
      {
        'Content-Type' => 'application/json',
      }
    end

    def base_url
      'https://www.googleapis.com/oauth2/v3'
    end

    def url(params)
      connection.build_url endpoint, url_params(params)
    end
  end
end
