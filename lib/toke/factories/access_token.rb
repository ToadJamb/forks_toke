FactoryGirl.define do
  factory :toke_access_token, :class => Toke::AccessToken do
    token { FactoryGirl.generate :toke_token }
    expires 3600

    factory :toke_expired_access_token do
      after(:build) do |access_token, evaluator|
        created_at = Time.now - access_token.expires - 2
        access_token.instance_variable_set :@created_at, created_at
      end
    end
  end
end
