module Toke
  class GetAccessTokenParams
    attr_reader :client_id, :client_secret, :refresh_token

    def initialize(params)
      @params = params

      @client_id     = normalize(:client_id)
      @client_secret = normalize(:client_secret)
      @refresh_token = normalize(:refresh_token)
    end

    def valid?
      !!(client_id && client_secret && refresh_token)
    end

    private

    def normalize(key)
      value = @params[key] || @params[key.to_s]
      value = nil if value && value.strip == ''
      value
    end
  end
end
