module Toke
  module Core
    def retrieve_token(params)
      GetAccessTokenCommand.execute params
    end
  end
end
