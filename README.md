Toke
====

[![Build Status](https://semaphoreci.com/api/v1/projects/f4fee9de-4c25-43dd-99af-0b4b3f6bf23b/459184/badge.svg)](https://semaphoreci.com/awesomenesstv/toke)

Retrieve OAuth tokens.


Setup
-----

	$ bundle install


Run Specs
---------

	$ bundle exec rake


### Regenerating VCR Cassettes

Check `config/dotenv/test.env` for the required keys.

Follow the instructions [here][atv-wiki-google-api-key]
to find the required values.

Place them in `config/dotenv/integration.env`.


Interface
---------

All public endpoints are exposed in Toke::Core.

Every response from the public API is wrapped in a `Response` object
that will always have the same interface regardless of request.
The `Response#data` attribute will be an object specific to the data requested.


### retrieve_token

retrieve_token returns a response object,
which will contain an access token in its data attribute.
The access token has a `token` property that is the string token value.

A rudimentary implementation is given below as an example:

```
def access_token
  @access_token = nil if !defined?(@access_token)

  if !@access_token || @access_token.expired?
    params = {
      :client_id     => client_id,
      :client_secret => client_secret,
      :refresh_token => refresh_token,
    }

    response = Toke.retrieve_token(params)
    @access_token = response.data if response.success?
  end

  @access_token && @access_token.token
end
```


Factories
---------

Toke includes FactoryGirl factories for your convenience.
Include them after requiring FactoryGirl:

	require 'toke/factories'


Deployment
----------

This project makes use of branches to manage deployment.
Pushing a new commit to the `production` branch
will also build and push this gem to RubyGems.


[atv-wiki-google-api-key]: https://www.github.com/awesomenesstv/wiki#google-api-key
